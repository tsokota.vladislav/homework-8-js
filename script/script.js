// 1. DOM - це "живе" полотно яке відображає реальну картину сторінки. За допомогою DOM можна вностити будь-які зміни в HTML теги,
// стилі та інше.

// 2. innerHTML - це властивість, що дозволяє встановлювати або отримувати HTML-вміст елемента у вигляді рядку
// innerText - це властивість, що дозволяє задавати або отримувати текстовий вміст елемента та його нащадків

// 3. "Вхідною точкою" в DOM є document
// Звернутися до елементів можна за допомогою даних методів:
// document.getElementById("id"); - по id елементу
// document.getElementsByTagName("tag"); - по назві тегу
// document.querySelector("ccs-selector"); - по першому CSS селектору елемента
// document.querySelectorAll("ccs-selector"); - по CSS селектору елементів
// document.getElementsByClassName("className"); - по назві класу
// Я вважаю, що найбільш універсальним є querySelectorAll("ccs-selector") так як він повертає всі елементи всередині, що задовольняють
// даному CSS-селектору.

let paragraph = document.getElementsByTagName("p");
for (let paragraphs of paragraph) {
  paragraphs.style.backgroundColor = "#ff0000";
}

let elemId = document.getElementById("optionsList");
console.log(elemId);
console.log(elemId.parentNode);
for (let byId of elemId.childNodes) {
  console.log(byId.nodeName, byId.nodeType);
}

// В hw завданні в третьому пункті помилка. Елемента з класом "testParagraph" НЕ існує. Існує з данною назвою тільки id.
// Даний пункт виконую згідно id="testParagraph"
let content = document.getElementById("testParagraph");
content.innerHTML = "This is a paragraph";

let elemLi = document.querySelectorAll(".main-header > div > ul> li");
for (let key of elemLi) {
  key.classList.add("nav-item");
}
console.log(elemLi);

let elemTitle = document.querySelectorAll(".section-title");
for (let elem of elemTitle) {
  elem.classList.remove("section-title");
}
